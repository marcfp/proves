#include "std_lib_facilities.h"
static int MAX=5;

void llegir_noms(vector<string>& name, int debug){
    string nom="1";
       // for(int i=0; i<MAX; ++i)
        //for(int i=0; i<=name.size(); ++i)
        int i=1;
        while(nom.compare("0")!=0)
        {
            cout << "Entra el nom (0 per acabar)" << i << " : ";
            cin >> nom;
            if(nom.compare("0")!=0){
                    name.push_back(nom);
                    i++;
            }
            
        }
        
}

void escriure_nom(vector<string>& name, int debug){
        
        if(debug==1)cout << "SÓC Dins d'escriure_nom ?" << endl; 
        cout <<  name.size() << endl;
        for(vector<string>::const_iterator i=name.begin(); i!=name.end();++i){ 
            cout << *i << endl; // this will print all the contents of *features*
        }
}

void llegir_edat(vector<double>& edat, vector<string>& name, int debug){
    double edatv;
    if(debug==1)cout << "SÓC Dins de llegir_edat ?" << endl; 
    vector<string>::const_iterator i_name=name.begin();
    vector<string> kk;
    for(int i=0; i<name.size(); ++i)
        {            
            cout << "Entra l'edat de " << *i_name << " : ";
            cin >> edatv;
            edat.push_back( edatv);
            //kk.push_back(make_pair(i_name, edatv));
            edatv=0;
            ++i_name;
        }
}

void escriure_edat(vector<double>& edat, int debug){
    if(debug==1)cout << "SÓC Dins de escriure_edat ?" << endl; 
    cout <<  edat.size() << endl;
        for(vector<double>::const_iterator i=edat.begin(); i!=edat.end();++i){ 
            cout << *i << endl; // this will print all the contents of *features*
        }
}

void escriure_ordenat(vector<double>& edat, vector<string>& name, int debug){
    int contador=0;
    vector<string> name_sense_tocar = name; 
    //cout << endl<< endl<< " HE D'USAR TEMPLATE'S!!!! https://duckduckgo.com/?q=c%2B%2B11+template+tutorial&t=canonical&ia=web" << endl<< endl;
    
    cout << endl << "abans d'ordenar name és : " << endl;
      for(int i=0; i!=name.size()-1;++i){
        for(int ie=0; ie!=edat.size();++ie){
            if(ie==i)cout << name[i] << " amb l'edat " << edat[ie] << endl;
        }
    }
  
    sort(name.begin(),name.end());
    cout << endl << "desprès d'ordenar name és : " << endl;
    for(vector<string>::const_iterator i=name.begin(); i!=name.end();++i){ 
            cout << *i << endl; // this will print all the contents of *features*
        }
        
    cout << endl << " copia_name :" << endl;
    for(vector<string>::const_iterator i=name_sense_tocar.begin(); i!=name_sense_tocar.end();++i){ 
            cout << *i << endl; // this will print all the contents of *features*
        }
    
    cout << endl << "arribo?" << endl;
    for(int i=0; i<name.size();i++){
        for(int nst=0; nst<name_sense_tocar.size();nst++){
            if(name[i].compare(name_sense_tocar[nst])==0) cout << "són iguals " << name[i] << " i " << name_sense_tocar[nst] << " i la seva edat és " << edat[nst] << endl;
            //telse cout << " name[i] = " << name[i] << " name_sense_tocar[nst] = " << name_sense_tocar[nst] << endl;             
        }
    }
}

int main(int argc, char **argv) {
    vector<string> name;
    vector<double> age;
    int debug=1;
    cout << "7. Read five names into a vector<string> name, then prompt the user for the ages of the people named and store the ages in a vector<double> age. Then print out the five (name[i],age[i]) pairs. Sort the names (sort(name.begin(),name.end())) and print out the (name[i],age[i]) pairs. The tricky part here is to get the age vector in the correct order to match the sorted name vector. Hint: Before sorting name, take a copy and use that to make a copy of age in the right order after sorting name. Then, do that exercise again but allowing an arbitrary number of names." << endl;
    cout << " Sort the names (sort(name.begin(),name.end())) and print out the (name[i],age[i]) pairs.\n\n Before sorting name, take a copy and use that to make a copy of age in the right order after sorting name. \nAbans d'ordenar el nom, feu una còpia i utilitzeu-la per fer una còpia de l'edat en l'ordre correcte després d'ordenar el nom." << endl;
    llegir_noms(name, debug);
    //escriure_nom(name, debug);
    llegir_edat(age, name, debug);
    //escriure_edat(age, debug);
    escriure_ordenat(age, name, debug);
    
    return 0;
}
