#include "std_lib_facilities.h"



class Date{
public:
    Date(){
            
            //string anydate;
        
            string anydatestring;
            int anydate=0, mesdate=0, diadate=0;
            cout << endl << "Creo la clase data" << endl;
            
            any=set_Any(anydatestring);
            mes=set_Mes();//anydate);
            dia=set_Dia( mesdate);
            
            //cout << " " << get_Dia() << " " << get_Mes()  << " " << get_Any()  << endl; 
    }
    ~Date(){
        
    }
    
    int set_Dia(int diamax){//string stringentrat, int diamax){
        int diaentrat;
        string stringentrat;
            do{
                
                cout << " Input day's number" << endl;
                getline(cin, stringentrat);
                diaentrat=atoi(stringentrat.c_str());
            }while(diaentrat<0 && diaentrat>diamax);
            dia=diaentrat;
            return dia;
    }
    int get_Dia(){
        return dia;
    }
    int set_Mes(){//string stringentrat){
        int monthentrat;
        string stringentrat;
            do{
                
                cout << " Input month's number" << endl;
                getline(cin, stringentrat);
                monthentrat=atoi(stringentrat.c_str());
            }while(monthentrat<1 && monthentrat>12);
            mes=monthentrat;
            return mes;
    }
    int get_Mes(){
        return mes;
    }
    int set_Any(string anyentrat){
        int anyen=1;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            do{
                
                cout << " Input year's number" << endl;
                getline(cin, anyentrat);
                anyen=atoi(anyentrat.c_str());
            }while(anyen<1900 && anyen>2100);
            any=anyen;
            return any;
    }
    int get_Any(){
        return any;
    }
    
private:    
    int dia=0;
    int mes=0;
    int any=0;    
};


class Book{
    public:
        Book(){
            
            cout << endl << "creació del llibre " << endl;
            conta++;
         
        }
        
        ~Book(){
                cout << endl << "destructor de llibre" << endl;
        }
        
        /*void set_Count(){
            count=0;
        }
        void suma_Count(){
            count++;
        }
        int get_Count(){
            return count;
        }*/
        void set_ISBN(string input){
            string n1;
            string n2;
            string n3;
            string x;
            int check=0;
            string resposta;
            do{
                cout << " Input ISBN of 3 numbers and one letter or number \n accept ISBNs only of the form n-n-n-x where n is an integer and x is a digit or a letter." << endl;
                //getline(cin,input);
                do{
                    cout << " First number : " ;
                    cin >> n1;
                    for(int i=0; i<n1.length()-1;++i){
                        if(isdigit(n1[i]<0)){
                            cout << "it's not a digit ->" << n1[i] << "<- it throws this error "  << endl;
                            check=1;
                        }
                    }
                    if(check!=0) {
                        do{
                            cout << " An error happens, do you want to retry ? ( answer y or n )" << endl;
                            cin >> resposta;
                        }while(resposta.compare("y")!=0 && resposta.compare("n")!=0);
                    }
                }while (check!=0);
                //cout << " Second number : " ;
                //cin >> n2;
                do{
                    cout << " Second number : " ;
                    cin >> n2;
                    for(int i=0; i<n2.length()-1;++i){
                        if(isdigit(n2[i]<0)){
                            cout << "it's not a digit ->" << n2[i] << "<- it throws this error "  << endl;
                            check=1;
                        }
                    }
                    if(check!=0) {
                        do{
                            cout << " An error happens, do you want to retry ? ( answer y or n )" << endl;
                            cin >> resposta;
                        }while(resposta.compare("y")!=0 && resposta.compare("n")!=0);
                    }
                }while (check!=0);
                //cout << " Third number : " ;
                //cin >> n3;
                do{
                    cout << " Third number : " ;
                    cin >> n3;
                    for(int i=0; i<n3.length()-1;++i){
                        if(isdigit(n3[i]<0)){
                            cout << "it's not a digit ->" << n3[i] << "<- it throws this error "  << endl;
                            check=1;
                        }
                    }
                    if(check!=0) {
                        do{
                            cout << " An error happens, do you want to retry ? ( answer y or n )" << endl;
                            cin >> resposta;
                        }while(resposta.compare("y")!=0 && resposta.compare("n")!=0);
                    }
                }while (check!=0);
                //cout << " Number or letter: " ;
                //cin >> x;
                do{
                    cout << " Number or letter: " ;
                    cin >> x;
                    for(int i=0; i<x.length()-1;++i){
                        if(isdigit(x[i]<0) || isalpha(x[i])<0){
                            cout << "it's not a digit ->" << x[i] << " and either a letter too <- it throws this error "  << endl;
                            check=1;
                        }
                    }
                    if(check!=0) {
                        do{
                            cout << " An error happens, do you want to retry ? ( answer y or n )" << endl;
                            cin >> resposta;
                        }while(resposta.compare("y")!=0 && resposta.compare("n")!=0);
                    }
                }while (check!=0);
                
                input=n1+"-"+n2+"-"+n3+"-"+x;
                
                cout << " This is what i typed = " << n1 << " - " << n2 << " - " << n3 << " - " << x << endl;
                
                cout << " An input looks like : " << input << " input.length() = " << input.length() << endl;
                
            }while(input.length()<7);
            ISBN=input;
        }
        
        void get_ISBN(){
            cout << " ISBN = " << ISBN << endl;    
        }
        string get_string_ISBN(){
            return ISBN;
        }
        void set_Titol(string input){
            
            do{
                cout << " Input Title of this book " << endl;
                getline(cin,input);
            }while(input.length()<=3);
            Titol=input;
        }
        
        void get_Titol(){
            cout << " Titol = " << Titol << endl;
        }
        string get_string_Titol(){
            return (Titol);
        }
        void set_Autor(string input){
            do{                
                cout << " Input Autor of this book " << endl;
                getline(cin,input);
            }while(input.length()<=3);
            Autor=input;
        }
        
        void get_Autor(){
        //string get_Autor(){
            cout << " Autor : " << Autor << endl;
            
           // return (Autor );
        }
        
        string get_string_Autor(){
            return (Autor);
        }   
        void set_Date(){
            Date date;            
        }
        
        void get_Date(){
            cout << " dia " << date.get_Dia() << " del mes "<< date.get_Mes() << " de l'any " << date.get_Any() << "\n";
        }
                        
        int get_date_Any(){
            return date.get_Any();
        }
        int get_date_Mes(){
            return date.get_Mes();
        }
        int get_date_Dia(){
            return date.get_Dia();
        }
        
        static int numero_llibres(void){
            return conta;
        }
        
        int set_Genre(){
             //   int genre;
                do{
                cout << " Please, input a Genre of this book " << endl ;                
                cout << "\t 1 <- Fiction Book " << endl;
                cout << "\t 2 <- NonFiction " << endl;
                cout << "\t 3 <- Periodical Book " << endl;
                cout << "\t 4 <- Biography Book " << endl;
                cout << "\t 5 <- Children Book " << endl << "\t : ";
                cin >> genre;
                }while(!isdigit(genre) && genre >5 && genre < 1 );
                cout << " Genre = " << genre << endl;
                
                return genre;
        }
        string get_Genre(){
                switch(genre)
                {
                    case 1: return " Fiction Book " ;
                    case 2: return " NonFiction Book " ;   
                    case 3: return " Periodical Book " ;   
                    case 4: return " Biography Book " ;   
                    case 5: return " Children Book " ;   
                    default : return " Non genre introduced ";
                }
        }
        friend ostream& operator << (ostream &out, Book& b1);
        enum Genre { fiction, nonfiction,periodical, biography, children };
private:
    string ISBN;
    string Titol;
    string Autor;
    // current date/time based on current system
    //time_t Now = time(0);
    Date date;
    static int conta;
    int genre;
    
    //DateTime date1; //=new DateTime(4,5,2012);
};

ostream& operator << (ostream &out , Book& b1){
    
    out << " Title : " << b1.get_string_Titol() << " Author : "  << b1.get_string_Autor() << " ISBN : " << b1.get_string_ISBN() << " Genere : " << b1.get_Genre() << endl; 
    return out;
}

void llibres_biblioteca( Book& llibre){
    
        llibre.get_ISBN();
            
        llibre.get_Titol();
            
        llibre.get_Autor();
        
        llibre.get_Genre();
              
        llibre.get_Date();
           
}

void llibres_introduccio(Book& llibre){
    
        
        
        string isbn;
        
        llibre.set_ISBN(isbn);        
    
        string titol;
    
        llibre.set_Titol(titol);
             
        string autor;
    
        llibre.set_Autor(autor);                
        
        llibre.set_Genre();
        
        //llibre.set_Date();
                        
}

int Book::conta =0;

int main(int argc, char **argv) {
    int introduccio_llibres=0;
    
    
    //cout << "//com puc sobreescriure una classe array al metode per escriure per pantalla? tema 9 ex 6, Have a << print out the title, author, " << endl << "//and ISBN on separate lines " << endl << "//ostream& operator<<(std::ostream& os, const Book& llibre[0]){ " << endl << " //    return os << llibre[0].get_Titol() << endl << llibre[0].get_Autor() << endl << llibre[0].get_ISBN() << endl;" << endl << " //} " ;
 
    
    //cout << "5. This exercise and the next few require you to design and implement a Book class, such as you can imagine as part of software for a library. Class Book should have members for the ISBN, title, author, and copyright date. Also store data on whether or   not the book is checked out. Create functions for returning those data values. Create functions for checking a book in and out.Do simple validation of data entered into a Book; for example, accept ISBNs only of the form n-n-n-x where n is an integer and x is a digit or a letter. Store an ISBN as a string." << endl;

    //cout << "6. Add operators for the Book class. Have the == operator check whether the ISBN numbers are the same for two books. Have != also compare the ISBN numbers. Have a << print out the title, author, and ISBN on separate lines. " << endl;
    
    cout << "7. Create an enumerated type for the Book class called Genre. Have the types be fiction, nonfiction, periodical, biography, and children. Give each book a Genre and make appropriate changes to the Book constructor and member functions. " << endl;
    
    //cout << endl << " proves " << endl;
    do{
        cout << "Quants llibres vols introduir ?" << endl;
        cin >> introduccio_llibres;
        if(cin.fail()){
            cout << "Not a number " << endl;
            cin.clear();   
            cin.ignore();
            introduccio_llibres=0;     
        }
    }while( introduccio_llibres==0);
    
    cout << " Input book's wroten date : " << endl;
    Book llibres[introduccio_llibres];
    
    //llibres[0].set_Count();
    
    cout << " Creem els llibres " << endl;
    for(int i=0; i<introduccio_llibres; ++i){
        llibres_introduccio(llibres[i]);
    }
    cout << " Els llibres entrats són :" << endl << endl;
    cout << "##########################################################################################################" << endl;
    cout << "##########################################################################################################" << endl;
    cout << "##########################################################################################################" << endl;
    if(Book::numero_llibres()>1){
        for(int i=0; i<introduccio_llibres; ++i){       
            llibres_biblioteca(llibres[i]);
            cout << " genere del llibre " << llibres[i].get_Genre() << endl;
            cout << " llibres[" << i << "].get_date_Any() = " << llibres[i].get_date_Any()<< endl;
            cout << "##########################################################################################################" << endl;
        cout << "##########################################################################################################" << endl;
        cout << "##########################################################################################################" << endl;
        }   
        cout << endl << endl << endl;
        
        
        //if((llibres[0]==llibres[1])) cout << "(llibres[0]=l 2=llibres[1]) = CERT" << endl;    
        //else cout << "(llibres[0]!=llibres[1]) NO SÓN IGUALS!!" << endl;
        
        if(llibres[0].get_string_Autor().compare(llibres[1].get_string_Autor())==0) cout << "(llibres[0].get_string_Autor().compare(llibres[1].get_string_Autor()) = CERT" << endl;    
        else cout << "(llibres[0].get_Autor().compare(llibres[1].get_Autor()) NO SÓN IGUALS!!" << endl;
        
        if((llibres[0].get_string_ISBN()).compare(llibres[1].get_string_ISBN())==0) cout << " (llibres[0].get_ISBN()).compare(llibres[1].get_ISBN()) = CERT, tenen el mateix ISBN " << endl;
            else cout << " llibres[0].get_ISBN().compare(llibres[1].get_ISBN()) SÓN DIFERENTS, NO tenen el mateix ISBN " << endl;
            
        //cout << " llibres[0].get_Count() = " << llibres[0].get_Count() << endl;    
        //cout << " llibres[1].get_Count() = " << llibres[1].get_Count() << endl;
        //cout << "llibre.conta = " << Book::conta;
        
        for(int i=0; i<Book::numero_llibres(); i++){
            cout << "imprimeix llibre [" << i << "] = "  <<llibres[i] ;
        }
    }
    else {
            cout << " llibres[0].get_string_ISBN() = " << llibres[0].get_string_ISBN() << " llibres[0].get_string_Autor() = " << llibres[0].get_string_Autor() << " llibres[0].get_string_Titol() = " <<  llibres[0].get_string_Titol() << endl;
            cout << " genere del llibre " << llibres[0].get_Genre() << endl;
    }
    return (0);
}

